# axb

Repository contains solvers for a linear system.

## Installation

 ``pip install git+https://gitlab.inria.fr/mshpakov/axb.git``
 
## Quick start

- GMRES
    ```python
    import axb
    import numpy
    
    n = 10
    A = numpy.random.randn(n, n)
    x = numpy.random.randn(n, 1)
    b = numpy.matmul(A, x)
    
    x_hat, info = axb.gmres(A, b, K_dim=n)
    assert info == 0
    assert numpy.allclose(x, x_hat)
    ```