import unittest
from axb import krylov


class TestKrylovSolvers(unittest.TestCase):

    def test_gmres_cpu(self):
        import numpy as np
        from scipy.sparse import csc_matrix

        A = csc_matrix([[3, 2, 0], [1, -1, 0], [0, 5, 1]], dtype=float)
        b = np.array([[2], [4], [-1]], dtype=float)
        x, info = krylov.gmres(A, b)
        self.assertTrue(np.allclose(A.dot(x), b))

    def test_gmres_cpu_complex(self):
        import numpy as np
        from scipy.sparse import csc_matrix

        A = csc_matrix([
            [3 + 5j, 2 - 1j, 0 + 0j],
            [1 - 7j, -1 + 3j, 0 + 0j],
            [0 + 0j, 5 + 1j, 1 - 2j],
        ], dtype=complex)
        x = np.array([[2 + 3j], [4 - 1j], [-1 + 3j]], dtype=complex)
        b = A.dot(x)
        x, info = krylov.gmres(A, b)
        self.assertTrue(np.allclose(A.dot(x), b))

    def test_gmres_gpu(self):
        import cupy as cp
        from cupyx.scipy.sparse import csr_matrix

        A = cp.array([[3, 2, 0], [1, -1, 0], [0, 5, 1]], dtype=float)
        A = csr_matrix(A)
        b = cp.array([[2], [4], [-1]], dtype=float)
        x, info = krylov.gmres(A, b)
        self.assertTrue(cp.allclose(A.dot(x), b))

    def test_gmres_gpu_complex(self):
        import cupy as cp
        from cupyx.scipy.sparse import csr_matrix

        A = cp.array([
            [3 + 5j, 2 - 1j, 0 + 0j],
            [1 - 7j, -1 + 3j, 0 + 0j],
            [0 + 0j, 5 + 1j, 1 - 2j],
        ], dtype=complex)
        A = csr_matrix(A)
        x = cp.array([[2 + 3j], [4 - 1j], [-1 + 3j]], dtype=complex)
        b = A.dot(x)
        x, info = krylov.gmres(A, b)
        self.assertTrue(cp.allclose(A.dot(x), b))

    def test_gmres_helmnet(self):
        import torch
        import cupy as cp
        from helmnet import IterativeSolver
        from helmnet.dataloaders import get_dataset
        from axb.interface import HelmholtzOperator

        solver = IterativeSolver.load_from_checkpoint(
            "/home/maksym/Work/INRIA/repos/gitlab/helmnet-exp/checkpoints/trained_weights.ckpt",
            strict=False,
            test_data_path=None,
        )
        solver.freeze()  # To evaluate the model without changing it
        solver.to('cuda')
        testset = get_dataset("/home/maksym/Work/INRIA/repos/gitlab/helmnet-exp/datasets/splitted_96/testset.ph")
        sos_map = testset.all_sos_numpy[0]

        b = solver.source.data[0]
        b = torch.complex(b[0], b[1])
        b = torch.reshape(b, (-1, 1))
        b = cp.asarray(b)

        A = HelmholtzOperator(solver, sos_map)
        x, info = krylov.gmres(A, b, K_dim=50, restarts=100, tol=1e-12)
        self.assertTrue(cp.allclose(A(x), b))

    def test_gmres_gpu_helmnet_preconditioner(self):
        import torch
        import cupy as cp
        from helmnet import IterativeSolver
        from axb.interface import HelmholtzOperator
        from axb.interface import NormHybridNetOperator
        from helmnet.dataloaders import get_dataset

        solver = IterativeSolver.load_from_checkpoint(
            "/home/maksym/Work/INRIA/repos/gitlab/helmnet-exp/checkpoints/trained_weights.ckpt",
            strict=False,
            test_data_path=None,
        )
        solver.freeze()  # To evaluate the model without changing it
        solver.to('cuda')
        testset = get_dataset("/home/maksym/Work/INRIA/repos/gitlab/helmnet-exp/datasets/splitted_96/testset.ph")
        sos_map = testset.all_sos_numpy[0]

        b = solver.source.data[0]
        b = torch.complex(b[0], b[1])
        b = torch.reshape(b, (-1, 1))
        b = cp.asarray(b)

        A = HelmholtzOperator(solver, sos_map)
        M = NormHybridNetOperator(solver, sos_map, norm_mask=[False, True])
        x, info = krylov.gmres(A, b, M=M, M_type="xr", K_dim=50, restarts=50, tol=1e-12)
        self.assertTrue(cp.allclose(A(x), b))
