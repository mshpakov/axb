from . import typedef


def is_gpu_backend(backend_name):
    return backend_name == typedef.GPU_BACKEND_NAME


def is_cpu_backend(backend_name):
    return backend_name == typedef.CPU_BACKEND_NAME


def make_system(A, M, x0, b):
    from .interface import ascontextualoperator

    A = ascontextualoperator(A)
    xp = A.backend
    dim = A.input_dim
    dtype = A.dtype

    b = xp.asarray(b)
    b = b.reshape(-1, 1)
    b = b.astype(dtype)

    if x0 is None:
        x = xp.zeros((dim, 1), dtype=dtype)
    else:
        x = xp.asarray(x0, dtype=dtype)
        x = x.reshape(-1, 1)

    if M is None:
        if A.backend_name == typedef.CPU_BACKEND_NAME:
            from scipy.sparse.linalg.interface import IdentityOperator
        else:
            from cupyx.scipy.sparse.linalg._interface import IdentityOperator
        M = IdentityOperator((dim, dim), dtype)

    M = ascontextualoperator(M)
    if A.dtype != M.dtype:
        raise ValueError(f"Data types of A and M do not coincide. {A.dtype} != {M.dtype}")

    if A.backend_name != M.backend_name:
        raise ValueError(f"Backends of A and M do not coincide. {A.backend_name} != {M.backend_name}")
    return A, M, x, b


def get_backend_name(A):
    from numpy import ndarray
    from scipy.sparse import spmatrix
    from scipy.sparse.linalg import LinearOperator

    if isinstance(A, (ndarray, spmatrix, LinearOperator)):
        return typedef.CPU_BACKEND_NAME
    else:
        from cupy import ndarray
        from cupyx.scipy.sparse import spmatrix
        from cupyx.scipy.sparse.linalg import LinearOperator

        if isinstance(A, (ndarray, spmatrix, LinearOperator)):
            return typedef.GPU_BACKEND_NAME
        else:
            raise ValueError(f"Invalid data for parameter A. "
                             f"Expected to be sparse or dense matrix or LinearOperator, but found '{type(A)}'.")


def get_backend_by_name(name):
    if is_cpu_backend(name):
        import numpy

        return numpy
    elif is_gpu_backend(name):
        import cupy

        return cupy
    else:
        raise ValueError(f"Invalid name for backend. "
                         f"Must be either 'numpy' or 'cupy' but found {name}.")


def make_update_hw(V, H, _H_cpu, xp, is_gpu_used, mode="mgs"):

    if mode == "cgs":
        # Classical Gram–Schmidt
        def update_hw(w, j):
            H[:j + 1, j:j + 1] = xp.matmul(V[:, :j + 1].T.conj(), w)
            w -= xp.matmul(V[:, :j + 1], H[:j + 1, j:j + 1])
            H[j + 1, j] = xp.linalg.norm(w)
            if is_gpu_used:
                _H_cpu[:j + 2, j:j + 1] = xp.asnumpy(H[:j + 2, j:j + 1])
            return H[j + 1, j], w
    elif mode == "mgs":
        # Modified Gram–Schmidt
        def update_hw(w, j):
            for d in range(0, j + 1):
                H[d, j] = xp.dot(V[:, d:d + 1].conj().T, w)
                w = w - H[d, j] * V[:, d:d + 1]
            H[j + 1, j] = xp.linalg.norm(w)
            if is_gpu_used:
                _H_cpu[:j + 2, j:j + 1] = xp.asnumpy(H[:j + 2, j:j + 1])
            return H[j + 1, j], w
    else:
        raise NotImplementedError

    return update_hw


def make_x_update(H, Z, e, xp):
    import numpy
    # from scipy.linalg import qr
    # from scipy.linalg import qr_insert
    # from scipy.linalg import solve_triangular

    def x_update(j):
        # Q, R = qr(H[:j + 2, :j + 1], mode="economic")
        # h = numpy.matmul(Q.conj().T, e[:j + 2, numpy.newaxis])
        # y = solve_triangular(R, h)
        y = numpy.linalg.lstsq(H[:j + 2, :j + 1], e[:j + 2, numpy.newaxis], rcond=None)[0]
        y = xp.array(y)
        return Z[:, :j + 1] @ y

    return x_update


def complex_to_real2(x, xp):
    x_re = xp.real(x)
    x_im = xp.imag(x)
    return xp.concatenate(
        [
            x_re[xp.newaxis],
            x_im[xp.newaxis],
        ],
        axis=0,
    )
