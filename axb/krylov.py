import numpy
from .utils import make_system
from .utils import make_x_update
from .utils import make_update_hw
from .typedef import GPU_BACKEND_NAME


def gmres(
        A,
        b,
        x0=None,
        M=None,
        M_type="v",
        tol=1e-8,
        K_dim=None,
        restarts=None,
        callback=None,
        callback_type=None,
        mode="mgs"):
    """ Uses Generalized Minimal RESidual iteration to solve ``Ax = b``.

    Algorithm supports CPU and GPU computations.

    Args:
        A (ndarray, spmatrix or LinearOperator): The real or complex
            matrix of the linear system with shape ``(n, n)``. ``A`` must be
            :class:`numpy.ndarray` or :class:`cupy.ndarray`,
            :class:`scipy.sparse.spmatrix` or :class:`cupyx.scipy.sparse.spmatrix`,
            :class:`scipy.sparse.linalg.LinearOperator` or :class:`cupyx.scipy.sparse.linalg.LinearOperator`.
        b (numpy.ndarray, cupy.ndarray): Right hand side of the linear system with shape
            ``(n,)`` or ``(n, 1)``.
        x0 (numpy.ndarray, cupy.ndarray): Starting guess for the solution.
        tol (float): Tolerance for convergence.
        restarts (int): Number of restarts.
        K_dim (int): Dimension of Krylov subspace. Larger values
            increase iteration cost, but may be necessary for convergence.
        M (ndarray, spmatrix or LinearOperator): Preconditioner for ``A``.
            The preconditioner should approximate the inverse of ``A``.
            ``M`` must be :class:`numpy.ndarray` or :class:`cupy.ndarray`,
            :class:`scipy.sparse.spmatrix` or :class:`cupyx.scipy.sparse.spmatrix`,
            :class:`scipy.sparse.linalg.LinearOperator` or :class:`cupyx.scipy.sparse.linalg.LinearOperator`.
        M_type (str): The type of the preconditioner that is given.
            - If M_type="v" then the preconditioner is applied to the basis vector.
            - If M_type="0v" then the preconditioner takes two arguments: zero vector and basis vector.
            - If M_type="xr" then the preconditioner takes two arguments:
                current approximation ``x`` and a residual ``r`` vectors.
                Acts as a flexible preconditioner since ``x`` and ``r`` change at each iteration.
        callback (function): User-specified function to call on every restart.
            It is called as ``callback(arg)``, where ``arg`` is selected by
            ``callback_type``.
        callback_type (str): 'x' or 'pr_norm'. If 'x', the current solution
            vector is used as an argument of callback function. if 'pr_norm',
            relative (preconditioned) residual norm is used as an argument.
        mode (str): 'cgs' or 'mgs' which is classical or modified Gram-Schmidt to be used.
    Returns:
        tuple:
            It returns ``x`` (numpy.ndarray, cupy.ndarray) and ``info`` (int) where ``x`` is
            the converged solution and ``info`` provides convergence
            information.
    """

    A, M, x, b = make_system(A, M, x0, b)

    xp = A.backend
    dtype = A.dtype
    is_gpu_used = A.backend_name == GPU_BACKEND_NAME

    n = xp.shape(b)[0]
    if K_dim is None or K_dim > n:
        K_dim = n
    if not restarts:
        restarts = n // K_dim + 1

    if callback_type is None:
        callback_type = 'pr_norm'
    if callback_type not in ('x', 'pr_norm', 'pr_norm_iter', 'zv'):
        raise ValueError(f'Unknown callback_type: {callback_type}')
    if callback is None:
        callback_type = None

    V = xp.zeros((n, K_dim), dtype=dtype)
    Z = xp.zeros((n, K_dim), dtype=dtype)
    H = xp.zeros((K_dim + 1, K_dim), dtype=dtype)
    _H_cpu = numpy.zeros((K_dim + 1, K_dim), dtype=dtype)
    H_cpu = _H_cpu if is_gpu_used else H
    e = numpy.zeros((K_dim + 1,), dtype=dtype)
    _x = x.copy()

    update_hw = make_update_hw(V, H, _H_cpu, xp, is_gpu_used, mode)
    x_update = make_x_update(H_cpu, Z, e, xp)
    b_norm = xp.linalg.norm(b)
    info = 1

    # restart loop
    with M:
        for m in range(restarts):
            _r = r = b - A(x)

            _r_norm = r_norm = xp.linalg.norm(r)
            if callback_type == 'x':
                callback(x)
            elif callback_type in ['pr_norm', 'pr_norm_iter']:
                callback(r_norm / b_norm)

            # convergence check at restart
            if r_norm < tol * b_norm:
                info = 0
                break

            v = r / r_norm
            V[:, :1] = v
            e[0] = r_norm

            # Arnoldi iteration
            for d in range(K_dim):
                if M_type == "v":
                    z = M(v)
                elif M_type == "0v":
                    z = M(xp.zeros_like(v), v)
                elif M_type == "xr":
                    z = M(_x, _r)
                else:
                    raise ValueError(f"M type is invalid. Available values are 'v', '0v', 'xr', bu found '{M_type}'.")
                if callback_type == "zv":
                    callback([z, v])

                Z[:, d:d + 1] = z
                w = A(z)

                h, w = update_hw(w, d)
                if d < K_dim - 1:
                    v = w / h
                    V[:, d + 1:d + 2] = v
                if M_type == "xr" or callback_type == "pr_norm_iter":
                    _x = x + x_update(d)
                    _r = b - A(_x)
                    _r_norm = xp.linalg.norm(_r)
                    if d < K_dim - 1 or m == restarts - 1:
                        if callback_type == "pr_norm_iter":
                            callback(_r_norm / b_norm)
                    # convergence check inside restart window
                    if _r_norm < tol * b_norm:
                        info = 0
                        break
            # if we have converged inside restart window
            if info == 0:
                x = _x
                break
            x += x_update(K_dim - 1)
        return x, info
