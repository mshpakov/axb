import abc
import torch
import numpy
from . import typedef
from .utils import get_backend_name
from .utils import complex_to_real2
from .utils import get_backend_by_name


class ContextualOperator:

    def __init__(self, op):
        self._op = op
        self._xp = self._get_backend(op)
        self._backend_name = self._xp.__name__

    @property
    def backend(self):
        return self._xp

    @property
    def backend_name(self):
        return self._backend_name

    @property
    @abc.abstractmethod
    def dtype(self):
        pass

    @property
    @abc.abstractmethod
    def input_dim(self):
        pass

    @staticmethod
    @abc.abstractmethod
    def _get_backend(op):
        """ Returns backend of given operation. """

    @abc.abstractmethod
    def apply(self, *args, **kwargs):
        """ Applies nonlinear operator to inputs """

    def __call__(self, *args, **kwargs):
        return self.apply(*args, **kwargs)

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        pass


class ContextualLinearOperator(ContextualOperator):

    def apply(self, x):
        return self._op.matvec(x)

    @property
    def input_dim(self):
        return self._op.shape[0]

    @property
    def dtype(self):
        return self._op.dtype

    @staticmethod
    def _get_backend(op):
        from scipy.sparse.linalg import LinearOperator

        if isinstance(op, LinearOperator):
            return get_backend_by_name(typedef.CPU_BACKEND_NAME)
        else:
            from cupyx.scipy.sparse.linalg import LinearOperator

            if isinstance(op, LinearOperator):
                return get_backend_by_name(typedef.GPU_BACKEND_NAME)
            else:
                raise ValueError(f"op must be either scipy or cupy "
                                 f"LinearOperator instance, but found '{type(op)}'")


class TorchContextualOperator(ContextualOperator, metaclass=abc.ABCMeta):

    def apply(self, *args, **kwargs):
        self._op.forward(*args, **kwargs)

    @staticmethod
    def _get_backend(op):
        if op.device.type == typedef.CUDA_DEVICE_TYPE:
            return get_backend_by_name(typedef.GPU_BACKEND_NAME)
        return get_backend_by_name(typedef.CPU_BACKEND_NAME)


class ComplexUNetOperator(TorchContextualOperator):

    def __init__(self, op, input_shape=(96, 96), alpha=1.):
        super().__init__(op)
        self.input_shape = input_shape
        self.alpha = alpha

    @property
    def dtype(self):
        return "complex128"

    def apply(self, x):
        x = torch.as_tensor(
            complex_to_real2(
                self.alpha * x.reshape(*self.input_shape),
                self._xp,
            ),
            device=self._op.device,
        ).unsqueeze(0).float()
        with torch.no_grad():
            h = self._op.forward(x)[0]
        h = torch.complex(h[0], h[1]) / self.alpha
        h = self._xp.asarray(h)
        h = h.reshape(-1, 1).astype(self.dtype)
        return h


class HybridNetOperator(TorchContextualOperator):

    def __init__(self, iterative_solver, sos_map):
        super().__init__(iterative_solver)
        self._sos_map = sos_map
        self.reset()

    @property
    def dtype(self):
        return "complex128"

    @property
    def input_dim(self):
        return numpy.prod(self._sos_map.shape)

    def reset(self):
        sos_maps = torch.tensor(
            self._sos_map[self._xp.newaxis, self._xp.newaxis],
            device=self._op.device,
        ).float()

        _, initial_guess = self._op.get_initials(sos_maps)
        self._op.f.clear_states(initial_guess)

    def apply(self, *args):
        args = [
            torch.as_tensor(
                complex_to_real2(x.reshape(*self._sos_map.shape), self._xp),
                device=self._op.device,
            ).unsqueeze(0)
            for x in args
        ]
        h = self._op.build_inputs(*args)
        h = self._op.compute_step(h.float())[0]
        h = torch.complex(h[0], h[1])
        h = self._xp.asarray(h)
        h = h.reshape(-1, 1)
        h = h.astype(self.dtype)
        return h

    def __exit__(self, *exc):
        self.reset()


class NormHybridNetOperator(HybridNetOperator):

    def __init__(self, iterative_solver, sos_map, norm_mask):
        super().__init__(iterative_solver, sos_map)
        self._norm_mask = norm_mask

    def apply(self, *args):
        xp = self._xp
        mask = self._norm_mask
        if len(args) != len(mask):
            raise ValueError("Number of inputs does not coincide with the size of normalization mask.")
        args = tuple([
            x / xp.linalg.norm(x) if normalize else x
            for x, normalize in zip(args, mask)
        ])
        return super().apply(*args)


class HelmholtzOperator(TorchContextualOperator):

    def __init__(self, iterative_solver, sos_map):
        super().__init__(iterative_solver)
        self._sos_map = sos_map
        sos_maps = torch.tensor(
            sos_map[self._xp.newaxis, self._xp.newaxis],
            device=self._op.device,
        )
        self._k_sq, _ = self._op.get_initials(sos_maps)

    @property
    def dtype(self):
        return "complex128"

    @property
    def input_dim(self):
        return numpy.prod(self._sos_map.shape)

    def apply(self, x):
        x = torch.as_tensor(
            complex_to_real2(
                x.reshape(*self._sos_map.shape),
                self._xp,
            ),
            device=self._op.device,
        ).unsqueeze(0)
        h = self._op.linear_operator(x, self._k_sq)[0]
        h = torch.complex(h[0], h[1])
        h = self._xp.asarray(h)
        h = h.reshape(-1, 1)
        return h


def ascontextualoperator(A):
    if isinstance(A, ContextualOperator):
        return A
    if get_backend_name(A) == typedef.CPU_BACKEND_NAME:
        from scipy.sparse.linalg import aslinearoperator
    else:
        from cupyx.scipy.sparse.linalg import aslinearoperator
    linear_operator = aslinearoperator(A)
    return ContextualLinearOperator(linear_operator)
